name := """vsfi-scoreboard"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean,SbtWeb)
scalaVersion := "2.13.3"
pipelineStages := Seq(gzip)

libraryDependencies ++= Seq(
  javaJdbc,
  guice,
  javaWs
)

libraryDependencies += "org.mindrot" % "jbcrypt" % "0.3m"
libraryDependencies += "mysql" % "mysql-connector-java" % "8.0.21"



// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

