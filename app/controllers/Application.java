package controllers;

import io.ebean.Ebean;
import io.ebean.SqlRow;
import models.SolvedTask;
import models.Task;
import models.User;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import security.SecuredAction;
import views.html.index;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@With(SecuredAction.class)
public class Application extends Controller {
    public Result index(Http.Request req) {
        return ok(index.render(req.session().get("username").orElse(null)));
    }

    public Result scoreboard() {
        String scoresQuery = "SELECT t_user.username as team, SUM(t_task.score) as score, MAX(solved_task.created_date) as last_date " +
                "FROM solved_task,user as t_user,task as t_task WHERE " +
                "solved_task.team_id = t_user.id and solved_task.task_id=t_task.id GROUP BY team";

        List<SqlRow> scoreRows = Ebean.createSqlQuery(scoresQuery).findList();

        List<ScoreboardRecord> scores =
                scoreRows.stream().map(row -> new ScoreboardRecord(row.getString("team"),
                        row.getInteger("score"),
                        row.getDate("last_date"))).collect(Collectors.toList());

        String adScoreQuery = "SELECT t_user.username as team, SUM(additional_score.score) as adScore " +
                "FROM solved_task, additional_score, user as t_user WHERE solved_task.id=additional_score.solved_task_id " +
                "AND solved_task.team_id=t_user.id GROUP BY team;";
        List<SqlRow> adScoreRows = Ebean.createSqlQuery(adScoreQuery).findList();
        Map<String, Integer> adScore =
                adScoreRows.stream().collect(Collectors.toMap(
                        sqlRow -> sqlRow.getString("team"),
                        sqlRow -> sqlRow.getInteger("adScore")));
        for (ScoreboardRecord record : scores) {
            if (adScore.containsKey(record.team)) {
                record.score += adScore.get(record.team);
            }
        }
        List<SimpleScoreboard> list = scores.stream()
                .sorted(Comparator.comparing(ScoreboardRecord::getScore, (o1, o2) -> Integer.compare(o2,o1))
                        .thenComparing(ScoreboardRecord::getDate, Date::compareTo))
                .map(rec -> {
                    SimpleScoreboard simpleScoreboard = new SimpleScoreboard();
                    simpleScoreboard.total = rec.score;
                    simpleScoreboard.team = rec.team;
                    return simpleScoreboard;
                }).collect(Collectors.toList());
        return ok(Json.toJson(list));
    }

    public Result teamScoreboard(String teamname) {
        List<SolvedTask> tasks = Ebean.createQuery(SolvedTask.class).where().eq("team",
                Ebean.createQuery(User.class).where().eq("username", teamname).findOne()).findList();
        Map<String, F.Tuple<Integer, List<F.Tuple<String, Integer>>>> extendedJson = tasks.stream().collect(Collectors.toMap(
                (t) -> t.task.title,
                (t) -> F.Tuple(t.task.score, t.scoreList.stream().map(
                        (s) -> F.Tuple(s.description, s.score)).collect(Collectors.toList()))));
        return ok(Json.toJson(extendedJson));
    }

    public Result task() {
        List<Task> taskList = Ebean.find(Task.class).orderBy().asc("id").findList();
        return ok(Json.toJson(IntStream.range(0, taskList.size()).boxed()
                .collect(Collectors.toMap(index -> "" + (index + 1), index -> taskList.get(index).title))));
    }

    public Result singleTask(Integer sequenceNumber) {
        Task task = Ebean.createQuery(Task.class).orderBy().asc("id")
                .findList().get(sequenceNumber - 1);
        return ok(Json.toJson(task));
    }

    private static class SimpleScoreboard {
        public String team;
        public int total;
    }

    private static class ScoreboardRecord {
        private String team;
        private int score;
        private Date date;

        public ScoreboardRecord(String team, int score, Date date) {
            this.team = team;
            this.score = score;
            this.date = date;
        }

        public String getTeam() {
            return team;
        }

        public void setTeam(String team) {
            this.team = team;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }
    }
}
