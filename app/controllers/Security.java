package controllers;

import com.google.inject.Inject;
import exception.WrongPasswordException;
import io.ebean.Ebean;
import models.User;
import org.mindrot.jbcrypt.BCrypt;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import security.SecuredAction;

/**
 * Created by Alex on 18.06.2015.
 */

public class Security extends Controller {
    private FormFactory formFactory;

    @Inject
    public Security(FormFactory formFactory) {
        this.formFactory = formFactory;
    }

    public Result login() {
        return ok(views.html.login.render(formFactory.form(UserFields.class)));
    }

    public Result doLogin(Http.Request req) {
        Form<UserFields> form = formFactory.form(UserFields.class).bindFromRequest(req);
        if (form.hasErrors()) {
            return ok(views.html.login.render(form));
        } else {
            User currentUser = Ebean.createQuery(User.class).where().eq("username", form.get().username)
                    .findOne();
            if (currentUser != null) {
                if (BCrypt.checkpw(form.get().password, currentUser.passwordHash)) {
                    return redirect("/").addingToSession(req, "username", currentUser.username);
                } else {
                    return redirect("/");
                }
            }
            return redirect("/");
        }
    }

    @With(SecuredAction.class)
    public Result logout() {
        return temporaryRedirect("/").withNewSession();
    }

    public static class UserFields {
        @Constraints.Required
        public String username;
        @Constraints.Required
        public String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String validate() {
            try {
                if (User.findUserByUsernameAndPassword(username, password) == null) {
                    return "Пользователь не существует";
                } else {
                    return null;
                }
            } catch (WrongPasswordException e) {
                return "Неверный пароль";
            }
        }
    }
}
