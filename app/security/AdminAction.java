package security;

import io.ebean.Ebean;
import models.User;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by Alex on 18.06.2015.
 */
public class AdminAction extends Action.Simple {

    @Override
    public CompletionStage<Result> call(Http.Request req) {
        CompletionStage<Result> unauthorizedResult = CompletableFuture
                .completedFuture(unauthorized("unauthorized"));
        String username = req.session().get("username").orElse(null);
        if (username != null) {
            User user = Ebean.createQuery(User.class).where().eq("username", username).findOne();
            if (user != null && user.isAdmin) {
                return delegate.call(req);
            } else {
                return unauthorizedResult;
            }
        }
        return unauthorizedResult;
    }
}
