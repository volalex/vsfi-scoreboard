package security;

import io.ebean.Ebean;
import models.User;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by Alex on 18.06.2015.
 */
public class SecuredAction extends Action.Simple {
    @Override
    public CompletionStage<Result> call(Http.Request req) {
        String username = req.session().getOptional("username").orElse(null);
        if (username != null && Ebean.createQuery(User.class).where().eq("username", username).findOne() != null) {
            return delegate.call(req);
        }
        else{
            return CompletableFuture.completedFuture(temporaryRedirect("/login"));
        }
    }
}
