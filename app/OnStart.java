import com.google.inject.AbstractModule;

/**
 * Created by Aleksey on 21.06.2016.
 */
public class OnStart extends AbstractModule {
    @Override
    protected void configure() {
        bind(ApplicationStart.class).asEagerSingleton();
    }
}
