import com.typesafe.config.Config;
import io.ebean.Ebean;
import models.User;
import play.Logger;
import play.api.Configuration;
import scala.Option;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Aleksey on 21.06.2016.
 */
@Singleton
public class ApplicationStart {
    @Inject
    public ApplicationStart(Configuration configuration) {
        try {
            if (Ebean.createQuery(User.class).where().eq("username", "root").findOne() == null) {
                User.createAdminUser("root", configuration.get("root.password", Config::getString));
            }
        } catch (Exception e) {
            Logger.of(ApplicationStart.class).error("Cannot create root user", e);
        }

    }
}
