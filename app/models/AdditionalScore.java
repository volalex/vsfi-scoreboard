package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.ebean.Model;

import javax.persistence.*;

/**
 * Created by Alex on 18.06.2015.
 */
@Entity
public class AdditionalScore extends Model {

    public AdditionalScore() {
    }

    public AdditionalScore(Integer score, String description) {
        this.score = score;
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    public Integer score;

    public String description;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    public SolvedTask solvedTask;
}
