package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import exception.WrongPasswordException;
import io.ebean.Ebean;
import io.ebean.Model;
import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.*;
import java.util.List;

/**
 * Created by alexey on 09.06.15.
 */
@Entity
public class User extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column(unique = true, nullable = false)
    public String username;

    @Column(nullable = false)
    public String passwordHash;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "team", fetch = FetchType.LAZY)
    public List<SolvedTask> solvedTasks;

    @Column(nullable = false)
    public Boolean isAdmin;


    public static User createTeamUser(String username,String password){
        User user = new User();
        user.isAdmin = false;
        user.username = username;
        user.passwordHash = BCrypt.hashpw(password,BCrypt.gensalt());
        user.save();
        return user;
    }

    public static User createAdminUser(String username,String password){
        User user = new User();
        user.isAdmin = true;
        user.username = username;
        user.passwordHash = BCrypt.hashpw(password,BCrypt.gensalt());
        user.save();
        return user;
    }

    public static User findUserByUsernameAndPassword(String username, String password) throws WrongPasswordException {
        User user = Ebean.find(User.class).where().eq("username", username).findOne();
        if (user!=null){
            if (BCrypt.checkpw(password,user.passwordHash)){
                return user;
            }
            else {
                throw new WrongPasswordException();
            }
        }
        else{
            return null;
        }
    }


}
