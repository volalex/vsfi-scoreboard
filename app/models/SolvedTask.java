package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Alex on 18.06.2015.
 */

@Entity
public class SolvedTask extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @JsonIgnore
    @ManyToOne
    public User team;

    @JsonIgnore
    @ManyToOne
    public Task task;

    @JsonIgnore
    @CreatedTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    public Date created;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "solvedTask", fetch = FetchType.EAGER)
    public List<AdditionalScore> scoreList;

    public static SolvedTask solveTask(User user, Task task, AdditionalScore... scores){
        SolvedTask solvedTask = new SolvedTask();
        solvedTask.team = user;
        solvedTask.task = task;
        solvedTask.save();
        for (AdditionalScore score:scores){
            score.solvedTask = solvedTask;
            score.save();
        }
        return solvedTask;
    }
}
