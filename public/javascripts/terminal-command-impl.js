angular.module('vsfi.command.impl', ['vsfi.command.tools'])

    .config(['commandBrokerProvider', function (commandBrokerProvider) {

        function syncGetJson(url){
            var request = new XMLHttpRequest();
            request.open("GET",url,false);
            request.send(null);
            if(request.status === 200){
                return JSON.parse(request.responseText);
            }
        }

        commandBrokerProvider.appendCommandHandler({
            command: 'version',
            description: ['Shows this software version.'],
            handle: function (session) {
                session.output.push({output: true, text: ['Version 2019.07'], breakLine: true});
            }
        });

        commandBrokerProvider.appendCommandHandler({
            command: 'clear',
            description: ['Clears the screen.'],
            handle: function (session) {
                session.commands.push({ command: 'clear' });
            }
        });



        commandBrokerProvider.appendCommandHandler({
            command: 'echo',
            description: ['Echoes input.'],
            handle: function (session) {
                var a = Array.prototype.slice.call(arguments, 1);
                session.output.push({ output: true, text: [a.join(' ')], breakLine: true });
            }
        });

        commandBrokerProvider.appendCommandHandler({
            command: 'eval',
            description: ['Evaluates input as javascript.','Example: eval alert(1)'],
            handle: function (session, param) {
                var a = Array.prototype.slice.call(arguments, 1);
                var param = eval(a.join(' '));
                param = param ? param.toString() : '';
                session.output.push({ output: true, text: [param], breakLine: true });
            }
        });

        commandBrokerProvider.appendCommandHandler({
            command: 'break',
            description: ['Tests how commands are broken down in segments.',"Example: break 'aaa aaa' aaa aaa"],
            handle: function (session) {
                var a = Array.prototype.slice.call(arguments, 1);
                session.output.push({ output: true, text: a, breakLine: true });
            }
        });

        commandBrokerProvider.appendCommandHandler({
           command: 'scoreboard',
           description: ["Competition scoreboard"],
           handle: function (session,team){
               if(team){
                   var json = syncGetJson("/scoreboard/"+team);
                   var outText = [];
                   if(!json||Object.keys(json).length==0){
                       throw new Error("No such team or team has no points")
                   }
                   for(var taskName in json){
                       outText.push("Task:"+taskName);
                       outText.push("Base score:"+json[taskName]["_1"]);
                       var ad_score = json[taskName]["_2"];
                       if(ad_score.length>0) {
                           outText.push("Additional score for this task:");
                       }
                       for(var i=0;i<ad_score.length;i++){
                           outText.push("\t Description:"+ad_score[i]["_1"]+" score:"+ad_score[i]["_2"])
                           outText.push("");
                       }
                       outText.push("-----------");
                   }
                   session.output.push({output: true, text: outText, breakLine: true});
               }
               else {

                   var json = syncGetJson("/scoreboard");
                   var outText = [];
                   for (var i = 0; i < json.length; i++) {
                       var record = json[i];
                       var str = record["team"];
                       if(record["team"].length > 5){
                           str+="\t";
                       }
                       else{
                           str+="\t\t";
                       }
                       str+=record["total"];
                       outText.push(str);
                   }
                   session.output.push({output: true, text: outText, breakLine: true});
               }}
        });

        commandBrokerProvider.appendCommandHandler({
            command: 'task',
            description: ["VSFI Tasks"],
            handle: function (session,taskid) {
                if(taskid){
                    var json = syncGetJson('/vsfi/'+taskid);
                    if(!json){
                        throw new Error("No such task")
                    }
                    var outText = [];
                    outText.push('');
                    outText.push(json.title);
                    outText.push(json.text);
                    outText.push("score:"+json.score);
                    outText.push('');
                    session.output.push({ output: true, text: outText, breakLine: true });
                }
                else{
                    var json = syncGetJson('/vsfi');
                    var outText = [];
                    console.log(json);
                    outText.push("Available tasks:");
                    console.log(Object.keys(json));
                    for (var num in json) {
                        if(json.hasOwnProperty(num)){
                            outText.push(num+". "+json[num]);
                        }
                    }
                    outText.push("");
                    outText.push("Enter 'task :task-id' for particular task description");
                    session.output.push({ output: true, text: outText, breakLine: false });
                }
            }
        });

        var helpCommandHandler = function () {
            var me = {};

            me.command = 'help';
            me.description = ['Provides instructions about how to use this terminal'];
            me.handle = function (session, cmd) {
                var list = commandBrokerProvider.describe();
                var outText = [];
                if (cmd) {
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].command == cmd) {
                            var l = list[i];
                            outText.push("Command help for: " + cmd);
                            for (var j = 0; j < l.description.length; j++) {
                                outText.push(l.description[j]);
                            }
                            break;
                        }
                    }
                    if(!outText.length)
                        outText.push("There is no command help for: " + cmd);
                }
                else {
                    outText.push("Available commands:");
                    for (var i = 0; i < list.length; i++) {
                        var str = "  " + list[i].command + "\t\t";
                        for (var j = 0; j < 3 && i + 1 < list.length; j++) {
                            var cmd = list[++i].command;
                            str += cmd + (cmd.length > 6 ? "\t" : "\t\t");
                        }
                        outText.push(str);
                    }
                    outText.push("");
                    outText.push("Enter 'help <command>' to get help for a particular command.");
                }
                session.output.push({ output: true, text: outText, breakLine: true });
            };
            return me;
        };
        commandBrokerProvider.appendCommandHandler(helpCommandHandler());
    }])

;